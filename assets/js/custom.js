$(document).ready(function () {
  $('.dropdown .toggle-btn').click(function () {
    $('.dropdown').toggleClass('open');
  });

  $('.nav-toggle').click(function () {
    $('.header-nav').toggleClass('open');
  });

  // typeahead js start
});

// $(document).ready(function() {
//   $(document).on('click', '.dropdown .toggle-btn', function() {
//     $(this).toggleClass('open');
//   });
//   $(document).on('click', 'body', function(e) {
//     if (!$(e.target).is('.open'))
//    .   $('.open').removeClass('open');
//   })
// });



var substringMatcher = function (strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;
    matches = [];
    substrRegex = new RegExp(q, 'i');
    $.each(strs, function (i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });
    cb(matches);
  };
};

var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
  'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
  'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
  'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
  'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
  'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
  'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
  'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];

$('#the-basics .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
  {
    name: 'states',
    source: substringMatcher(states),
    templates: {
      empty: ['<div class="empty-message">',
        'No Result Found', '</div>'].join('\n'),
      suggestion: function (item) {
        return '<span class="tt-country"><i class="icon-orange-light text-green"></i>' + item + '</span>';
      }
    }
  });





   
 var vmPostJob = new Vue({
  el: "#home",
  name: 'home',
  data() {
      return {
        AlljoksList: [],
        AllCategoryList:[],
        categoryName:'',
        colour:['btn-red','btn-orange','btn-pale-orange','btn-yellow','btn-green','btn-weird-green','btn-cyan'],
      }
  },
  created() {
    this.getAllJobks();
    this.getAllCategoey();
  },	
  methods: {

      getAllJobks: function (para='all') {
        this.categoryName = para;
        axios({
          method: 'get',
          url: 'https://api.chucknorris.io/jokes/search?query='+para,
          data: {  'query':'all'	},
            
          }).then(response => {	
            this.AlljoksList = response.data.result;
            for (i = 0; i < this.AlljoksList.length; ++i) {
              this.AlljoksList[i].categoryName= para;
            }
          }).catch(function (error) {
              console.log(error);
          });
       },
      getAllCategoey: function () {
         axios({
           method: 'get',
           url: 'https://api.chucknorris.io/jokes/categories',
           data: {	},
          
        }).then(response => {	
          this.AllCategoryList =  response.data;
        }).catch(function (error) {
            console.log(error);
        });
              
      },
      
  } 
});  

